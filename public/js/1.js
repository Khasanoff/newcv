(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],{

/***/ "./src/ts/xpage/select.ts":
/*!********************************!*\
  !*** ./src/ts/xpage/select.ts ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! ./EventListener */ "./src/ts/xpage/EventListener.ts"), __webpack_require__(/*! ./core */ "./src/ts/xpage/core.ts"), __webpack_require__(/*! ./Element */ "./src/ts/xpage/Element.ts")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, EventListener_1, core_1, Element_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var selectType;
    (function (selectType) {
        selectType[selectType["Single"] = 0] = "Single";
        selectType[selectType["Mutiple"] = 1] = "Mutiple";
    })(selectType || (selectType = {}));
    var optionsState;
    (function (optionsState) {
        optionsState[optionsState["opened"] = 0] = "opened";
        optionsState[optionsState["closed"] = 1] = "closed";
    })(optionsState || (optionsState = {}));
    var select = (function () {
        function select(select, settings) {
            this._type = selectType.Single;
            if (typeof select == "string")
                this._el = core_1.default.elementsGetter(select) ? core_1.default.elementsGetter(select)[0] : document.createElement("select");
            else if (select instanceof HTMLSelectElement)
                this._el = select;
            else {
                throw Error(select + " is not a select.");
                return;
            }
            if (!this._el.options.length)
                return;
            this.options = this._el.options;
            this.createSelect();
        }
        Object.defineProperty(select.prototype, "el", {
            get: function () {
                return this._el;
            },
            set: function (el) {
                this._el = el;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(select.prototype, "options", {
            set: function (options) {
                this._options = options;
                this._customOptions = new selectOptions(options);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(select.prototype, "value", {
            set: function (newVal) {
                if (newVal == this.el.value)
                    return;
                this.el.value = newVal;
                new EventListener_1.default(this.el).trigger("change");
            },
            enumerable: true,
            configurable: true
        });
        select.prototype.createSelect = function () {
            var fakeDiv = document.createElement("div");
            fakeDiv.innerHTML = this._customOptions.render();
            this._el.parentNode.insertBefore(fakeDiv.querySelector(".my-select__list-cont"), this._el.nextSibling);
            this.el.MySelect = this;
            this.bindEvents();
        };
        select.prototype.bindEvents = function () {
            var _this = this;
            new EventListener_1.default(this.el).add("mousedown", function (el, e) {
                e.preventDefault();
                if (el.classList.contains("js__opened"))
                    el.classList.remove("js__opened");
                else
                    el.classList.add("js__opened");
            });
            new EventListener_1.default("body").add("click", function (el, event) {
                var target = new Element_1.default(event.target), parent = _this.el.closest("div").querySelector(".my-select__list");
                if (!target.is(_this.el)
                    && !new Element_1.default(_this.el).has(target)
                    && !target.is(parent)
                    && !new Element_1.default(parent).has(target))
                    _this.el.classList.remove("js__opened");
            });
            new EventListener_1.default(this.el).add("focus", function (el, e) {
                el.classList.add("js__opened");
            });
            new EventListener_1.default(this.el).add("blur", function (el, e) {
                el.classList.remove("js__opened");
            });
            var $options = new Element_1.default(this.el.closest("div").querySelectorAll(".my-select__list-option"));
            new EventListener_1.default($options).add("click", function (el) {
                $options.removeClass("selected");
                el.classList.add("selected");
                _this.value = el.getAttribute("value") || "0";
                _this.el.classList.remove("js__opened");
            });
        };
        return select;
    }());
    var selectOptions = (function () {
        function selectOptions(_options) {
            this._options = _options;
            this._template = "<div class='my-select__list-cont'><ul class='my-select__list'>%options%</ul></div>";
            this._state = optionsState.closed;
            this._optionsArray = [];
            this.length = 0;
            this.length = this._options.length;
            if (this.length == 0)
                return;
            for (var i = 0; i < this.length; i++) {
                this._optionsArray.push(new selectOption(this._options[i]));
            }
        }
        Object.defineProperty(selectOptions.prototype, "state", {
            get: function () {
                return this.state;
            },
            set: function (newState) {
                this.state = newState;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(selectOptions.prototype, "renderedOptions", {
            get: function () {
                var optionsString = "";
                for (var _i = 0, _a = this._optionsArray; _i < _a.length; _i++) {
                    var option = _a[_i];
                    optionsString += " " + option.render();
                }
                return optionsString;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(selectOptions.prototype, "options", {
            get: function () {
                return this._optionsArray;
            },
            enumerable: true,
            configurable: true
        });
        selectOptions.prototype.replaceTemplateMarks = function (template) {
            return template.replace("%options%", this.renderedOptions);
        };
        selectOptions.prototype.render = function (template) {
            if (!template)
                return this.replaceTemplateMarks(this._template);
            else
                return this.replaceTemplateMarks(template);
        };
        return selectOptions;
    }());
    var selectOption = (function () {
        function selectOption(_el) {
            this._el = _el;
            this._template = "<li %attrs% class='my-select__list-option %classes%'>%text%</li>";
            this._text = this._el.text;
            this._value = this._el.value;
            this._attributes = this._el.attributes;
        }
        Object.defineProperty(selectOption.prototype, "text", {
            get: function () {
                return this._text;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(selectOption.prototype, "value", {
            get: function () {
                return this._value;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(selectOption.prototype, "attributes", {
            get: function () {
                return this._attributes;
            },
            enumerable: true,
            configurable: true
        });
        selectOption.prototype.getAttrsString = function () {
            ;
            var attrsObject = [], attrsString = "";
            for (var i = 0; i < this.attributes.length; i++) {
                attrsObject.push({
                    name: this.attributes[i].localName,
                    value: this.attributes[i].textContent
                });
                attrsString += " " + attrsObject[i].name + "='" + attrsObject[i].value + "'";
            }
            return attrsString;
        };
        selectOption.prototype.replaceTemplateMarks = function (template) {
            return template.replace("%attrs%", this.getAttrsString()).replace("%text%", this.text);
        };
        selectOption.prototype.render = function (template) {
            if (!template)
                return this.replaceTemplateMarks(this._template);
            else
                return this.replaceTemplateMarks(template);
        };
        return selectOption;
    }());
    exports.default = select;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ })

}]);
//# sourceMappingURL=1.js.map