<!doctype html>
<html lang="ru">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/main.css">
    <link rel="shortcut icon" href="/favicon-xpage.ico" type="image/x-icon"/>
    <title>Document</title>
</head>
<body class="main loading">
<header class="head">
    <div class="wrapper">
        <div class="head__wrap">
            <div class="head__logo"><a class="logo" href="#"><img src="img/logo.svg"/></a></div>
            <nav class="head__menu">
                <ul class="h-menu">
                    <li><a class="h-menu__link" href="#">Главная</a></li>
                    <li><a class="h-menu__link" href="#">Сотрудники</a></li>
                </ul>
            </nav>
            <div class="head__btns">
                <div class="h-btns">
                    <div class="h-btns__item"><a class="default-btn default-btn--bordered fancybox" href="#add-user"><svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M16 12.875C16 13.2202 15.7202 13.5 15.375 13.5H13.5V15.375C13.5 15.7202 13.2202 16 12.875 16C12.5298 16 12.25 15.7202 12.25 15.375V13.5H10.375C10.0298 13.5 9.75 13.2202 9.75 12.875C9.75 12.5298 10.0298 12.25 10.375 12.25H12.25V10.375C12.25 10.0298 12.5298 9.75 12.875 9.75C13.2202 9.75 13.5 10.0298 13.5 10.375V12.25H15.375C15.7202 12.25 16 12.5298 16 12.875ZM11 15.375C11 15.7202 10.7202 16 10.375 16H1.875C0.841064 16 0 15.1589 0 14.125V12.9688C0 11.8901 0.462646 10.8652 1.26917 10.1566C1.92212 9.58276 3.17261 8.7323 5.18286 8.29028C4.08423 7.44397 3.375 6.11584 3.375 4.625C3.375 2.07483 5.44983 0 8 0C10.5502 0 12.625 2.07483 12.625 4.625C12.625 7.17517 10.5502 9.25 8 9.25C4.56555 9.25 2.72571 10.5406 2.09424 11.0956C1.55774 11.567 1.25 12.2496 1.25 12.9688V14.125C1.25 14.4696 1.5304 14.75 1.875 14.75H10.375C10.7202 14.75 11 15.0298 11 15.375ZM8 8C9.86096 8 11.375 6.48596 11.375 4.625C11.375 2.76404 9.86096 1.25 8 1.25C6.13904 1.25 4.625 2.76404 4.625 4.625C4.625 6.48596 6.13904 8 8 8Z" fill="#2CAC6E"/>
                            </svg>
                            <span class="default-btn__text">Добавить сотрудника</span></a></div>
                    <div class="h-btns__item"><a class="default-btn fancybox" href="#log-in"><svg width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M4.12891 10.6219H4.07891V10.6719V12.8594C4.07891 13.7917 4.83722 14.55 5.76953 14.55H12.3594C13.2917 14.55 14.05 13.7917 14.05 12.8594V2.14063C14.05 1.20832 13.2917 0.45 12.3594 0.45L5.76953 0.45C4.83722 0.45 4.07891 1.20832 4.07891 2.14062V4.32812V4.37812H4.12891H5.22266H5.27266V4.32812V2.14062C5.27266 1.86671 5.49562 1.64375 5.76953 1.64375L12.3594 1.64375C12.6333 1.64375 12.8562 1.86671 12.8562 2.14063V12.8594C12.8562 13.1333 12.6333 13.3562 12.3594 13.3562H5.76953C5.49562 13.3562 5.27266 13.1333 5.27266 12.8594V10.6719V10.6219H5.22266H4.12891ZM5.78107 9.26933L5.74571 9.30469L5.78107 9.34004L6.55449 10.1135L6.58984 10.1488L6.6252 10.1135L9.20331 7.53536L9.23866 7.5L9.20331 7.46464L6.6252 4.88653L6.58984 4.85118L6.55449 4.88653L5.78107 5.65996L5.74571 5.69531L5.78107 5.73067L6.95352 6.90312H0H-0.05V6.95312L-0.05 8.04688V8.09688H0H6.95352L5.78107 9.26933Z" fill="white" stroke="white" stroke-width="0.1"/>
                            </svg>
                            <span class="default-btn__text">Войти</span></a></div>
                </div>
            </div>
        </div>
    </div>
</header>
<main id="content">
    <div class="wrapper">
        @yield('content')
    </div>
</main>
<footer class="footer">
    <div class="wrapper">
        <div class="footer__wrap">
            <div class="footer__dev"><a class="dev" href="#" target="_blank">
                    <img src="/img/logo.svg"/>
                </a>
            </div>
            <div class="footer__copy">
                <div class="copy">&copy; Все права защищены <?=date('Y')?></div>
            </div>
        </div>
    </div>
</footer>
@include('inc.addUser')
@include('inc.login')
@include('inc.print')



<script src="/js/vendors.js"></script>
<script src="/js/base.js"></script>
<script src="/js/xpage.js"></script>
<script src="/js/common.js"></script>
<script src="/js/moment.js"></script>
</body>
</html>