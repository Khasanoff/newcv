<div class="popup" id="log-in">
    <div class="popup__title">Войти в аккаунт</div>
    <div class="popup__content">
        <form action="#">
            <div class="form__inputs">
                <div class="form__inputs-input">
                    <div class="default-input">
                        <input class="default-input__input" type="text" required="" placeholder="Admin"/>
                        <label class="default-input__label">Логин</label>
                        <div class="default-input__error">
                            <div class="error-text"><!-- текст ошибки --></div>
                        </div>
                    </div>
                </div>
                <div class="form__inputs-input">
                    <div class="default-input">
                        <input class="default-input__input" type="password" required="" placeholder="*********"/>
                        <label class="default-input__label">Пароль</label>
                        <div class="default-input__error">
                            <div class="error-text"><!-- текст ошибки --></div>
                        </div>
                    </div>
                </div>
                <div class="form__inputs-input">
                    <div class="captcha">
                        <div class="captcha-input">
                            <div class="default-input">
                                <input class="default-input__input" type="text" required="" placeholder="Введите код с картинки"/>
                                <label class="default-input__label">Введите код с картинки</label>
                                <div class="default-input__error">
                                    <div class="error-text"><!-- текст ошибки --></div>
                                </div>
                            </div>
                        </div>
                        <div class="captcha-img"><img src="img/captcha.jpg"/></div>
                    </div>
                </div>
                <div class="form__submit-btn">
                    <input class="default-btn" type="submit" value="Войти в аккаунт"/>
                </div>
                <div class="form__submit-text">
                    <p>Забыли пароль? Вы можете его <a href="#">восставновить</a></p>
                </div>
            </div>
        </form>
    </div>
</div>