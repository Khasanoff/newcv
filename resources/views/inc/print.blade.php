<div class="popup" id="print-stats">
    <div class="popup__title">Распечатать статистику</div>
    <div class="popup__content">
        <form action="#">
            <div class="form__inputs">
                <div class="form__inputs-input">
                    <div class="default-input">
                        <div class="default-input__inputs">
                            <div class="default-input">
                                <select class="default-input__input default-input__input--select">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                            </div>
                            <div class="default-input">
                                <select class="default-input__input default-input__input--select">
                                    <option value="0">Январь</option>
                                    <option value="1">Январь</option>
                                    <option value="2">Январь</option>
                                    <option value="3">Январь</option>
                                    <option value="4">Январь</option>
                                    <option value="5">Январь</option>
                                </select>
                            </div>
                        </div>
                        <label class="default-input__label">С какой даты</label>
                    </div>
                </div>
                <div class="form__inputs-input">
                    <div class="default-input">
                        <div class="default-input__inputs">
                            <div class="default-input">
                                <select class="default-input__input default-input__input--select">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                            </div>
                            <div class="default-input">
                                <select class="default-input__input default-input__input--select">
                                    <option value="0">Январь</option>
                                    <option value="1">Январь</option>
                                    <option value="2">Январь</option>
                                    <option value="3">Январь</option>
                                    <option value="4">Январь</option>
                                    <option value="5">Январь</option>
                                </select>
                            </div>
                        </div>
                        <label class="default-input__label">По какою дату</label>
                    </div>
                </div>
                <div class="form__inputs-input">
                    <div class="default-input">
                        <select class="default-input__input default-input__input--select">
                            <option value="0">Неделя</option>
                            <option value="1">День</option>
                            <option value="2">Месяц</option>
                            <option value="3">Год</option>
                        </select>
                        <label class="default-input__label">Статистика за период </label>
                    </div>
                </div>
            </div>
            <div class="form__submit">
                <div class="form__submit-btn">
                    <input class="default-btn" type="submit" value="Распечатать статистику"/>
                </div>
            </div>
        </form>
    </div>
</div>