<div class="popup" id="add-user">
    <div class="popup__title">Добавить сотрудника</div>
    <div class="popup__content">
        <form action="#">
            <div class="form__inputs">
                <div class="form__inputs-input">
                    <div class="default-input">
                        <input class="default-input__input" type="text" required="" placeholder="Admin" value="Дмитрий Жердев"/>
                        <label class="default-input__label">Фио</label>
                        <div class="default-input__error">
                            <div class="error-text"><!-- текст ошибки --></div>
                        </div>
                    </div>
                </div>
                <div class="form__inputs-input">
                    <div class="default-input">
                        <input class="default-input__input" type="text" required="" placeholder="Руководитель отдела разработки" value="Руководитель отдела разработки"/>
                        <label class="default-input__label">Должность</label>
                        <div class="default-input__error">
                            <div class="error-text"><!-- текст ошибки --></div>
                        </div>
                    </div>
                </div>
                <div class="form__inputs-input">
                    <div class="default-input">
                        <select class="default-input__input default-input__input--select">
                            <option value="0">Отдел разработки</option>
                            <option value="1">Отдел разработки</option>
                            <option value="2">Отдел разработки</option>
                            <option value="3">Отдел разработки</option>
                            <option value="4">Отдел разработки</option>
                        </select>
                        <label class="default-input__label">Подразделение</label>
                        <div class="default-input__error">
                            <div class="error-text"><!-- текст ошибки --></div>
                        </div>
                    </div>
                </div>
                <div class="form__inputs-input">
                    <div class="default-input">
                        <input class="default-input__input default-input__input--file" type="file" id="file" multiple=""/>
                        <div class="default-input__input-filetext">
                            <input class="default-input__input" type="text" placeholder="Прикрепите фотографии" readonly=""/>
                            <label class="default-input__label--file" for="file"></label>
                        </div>
                        <label class="default-input__label">Фотографии для распознования</label>
                    </div>
                    <div class="ue-photos__list">
                        <div class="photos-list">
                            <div class="photos-list__item">
                                <div class="p-item">
                                    <figure class="p-item__img"><img src="/img/photos/removable-img.jpg"/></figure><a class="p-item__remove" href="#"></a>
                                </div>
                            </div>
                            <div class="photos-list__item">
                                <div class="p-item">
                                    <figure class="p-item__img"><img src="/img/photos/removable-img.jpg"/></figure><a class="p-item__remove" href="#"></a>
                                </div>
                            </div>
                            <div class="photos-list__item">
                                <div class="p-item">
                                    <figure class="p-item__img"><img src="/img/photos/removable-img.jpg"/></figure><a class="p-item__remove" href="#"></a>
                                </div>
                            </div>
                            <div class="photos-list__item">
                                <div class="p-item">
                                    <figure class="p-item__img"><img src="/img/photos/removable-img.jpg"/></figure><a class="p-item__remove" href="#"></a>
                                    <div class="p-item__loader">
                                        <div class="p-item__loader-bar">
                                            <div class="p-item__loader-progress" style="transform: scaleX(.75)"></div><!-- от 0 до 1 -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form__btns">
                    <div class="form__btns__item">
                        <div class="file-btn">
                            <input class="file-btn__input" type="file"/>
                            <label class="default-btn default-btn--bordered"></label>
                        </div>
                    </div>
                    <div class="form__btns__item">
                        <input class="default-btn" type="submit" value="Добавить"/>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>